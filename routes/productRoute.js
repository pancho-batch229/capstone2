/* -- PRODUCT ROUTE -- */
const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');


// --------------------------------------------------------------------------------- //
// Create Product
router.post('/post', (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin

	}

	productController.postProduct(data)
		.then(resultFromController => res.send(resultFromController));
});


// --------------------------------------------------------------------------------- //
// All Products
router.get('/all', (req, res) => {
	productController.allProducts()
		.then(resultFromController => res.send(resultFromController));
})


// --------------------------------------------------------------------------------- //
// All Products (ADMIN)
router.get('/all/admin', (req, res) => {
	productController.allProductsAdmin()
		.then(resultFromController => res.send(resultFromController));
})


// --------------------------------------------------------------------------------- //
// Product Details
router.get('/:productId', (req, res) => {
	productController.productDetail(req.params)
		.then(resultFromController => res.send(resultFromController));
})


// --------------------------------------------------------------------------------- //
// Update Product
router.put('/:productId', (req, res) => {
	const data = {
		product: req.body,
		id: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateProduct(data)
		.then(resultFromController => res.send(resultFromController));		
})


// --------------------------------------------------------------------------------- //
// Archive Products
router.put('/:productId/archive', auth.verify, (req, res) => {
	const data = {
		reqParams: req.params,
		product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin		
	}

	productController.archiveProduct(data)
        .then(resultFromController => res.send(resultFromController))		
})


module.exports = router;