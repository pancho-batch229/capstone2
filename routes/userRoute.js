/* -- USER ROUTE -- */
const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');


// --------------------------------------------------------------------------------- //
// User Registration
router.post('/register', (req, res) => {
	userController.registerUser(req.body)
		.then(resultFromController => res.send(resultFromController));
});


// --------------------------------------------------------------------------------- //
// All Users 
router.get("/:userId/all", auth.verify, (req, res) => {
	const data = {
		userId: req.params.userId,
		adminToken: auth.decode(req.headers.authorization).isAdmin
	}

	userController.allUsers(data)
		.then(resultFromController => res.send(resultFromController));
});


// --------------------------------------------------------------------------------- //
// User Login
router.post('/login', (req, res) => {
	userController.loginUser(req.body)
		.then(resultFromController => res.send(resultFromController));
});


// --------------------------------------------------------------------------------- //
// User Details
router.get('/:userId/profile', auth.verify, (req, res) => {
	let data = {
		userId: req.params.userId,
		userToken: auth.decode(req.headers.authorization).id
	}

	userController.userProfile(data)
		.then(resultFromController => res.send(resultFromController));
});


// --------------------------------------------------------------------------------- //
// Set Admin 
router.put('/:userId/set-admin', auth.verify, (req, res) => {
	const data = {
		reqParams: req.params,
		isAdmin: req.body.isAdmin,
		adminToken: auth.decode(req.headers.authorization).isAdmin
	}

	userController.setAdmin(data)
		.then(resultFromController => res.send(resultFromController));
});


module.exports = router;