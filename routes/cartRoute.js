/* -- CART ROUTE -- */
const express = require('express');
const router = express.Router();
const cartController = require('../controllers/cartController');
const auth = require('../auth');

// --------------------------------------------------------------------------------- //
// User Cart
router.get('/:userId/my-cart', auth.verify, (req, res) => {
	const userId = req.params.userId;
	const userToken = auth.decode(req.headers.authorization).id;

	cartController.userCart({ userId, userToken })
		.then(resultFromController => res.send(resultFromController));
})

// --------------------------------------------------------------------------------- //
// Add To Cart
router.post('/:userId', auth.verify, (req, res) => {
	const { productId, quantity } = req.body;
	const { userId } = req.params;
	const userToken = auth.decode(req.headers.authorization).id;

	cartController.addToCart({ productId, quantity, userToken, userId })
		.then(resultFromController => res.send(resultFromController));
})

// --------------------------------------------------------------------------------- //
// Update Quantity
router.put('/:userId/:productId', auth.verify, (req, res) => {
	const { productId, userId } = req.params;
	const { quantity } = req.body;
	const userToken = auth.decode(req.headers.authorization).id;

	cartController.updateProductQuantity({ productId, quantity, userToken, userId })
		.then(resultFromController => res.send(resultFromController));
})

// --------------------------------------------------------------------------------- //
// Remove Quantity
router.delete('/:userId/:productId', auth.verify, (req, res) => {
	const { userId, productId } = req.params;
	const userToken = auth.decode(req.headers.authorization).id;

	cartController.removeProductFromCart({ productId, userToken, userId })
		.then(resultFromController => res.send(resultFromController));
})


module.exports = router