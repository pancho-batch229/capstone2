/* -- SETUP MODULES / DEPENDENCIES -- */
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoute = require("./routes/userRoute"); 
const productRoute = require("./routes/productRoute");
const cartRoute = require("./routes/cartRoute");

/* -- SERVER SETUP -- */
const app = express();
const port = process.env.PORT || 4000;

app.listen(port, () => {
	console.log(`API is online on port ${port}`);
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "CONNECTION ERR0R!"));
db.once("open", () => console.log("The Waffle House Has Found It's New Host!"));

// Database Connection (MongoDB)
mongoose.set('strictQuery', true);
mongoose.connect("mongodb+srv://ashley:ashleyisdumb@capstone.wbtrxu9.mongodb.net/?retryWrites=true&w=majority",{
	useNewUrlParser:true,
	useUnifiedTopology:true
});

// --------------------------------------------------- //

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Routes
app.use("/user", userRoute);
app.use("/product", productRoute);
app.use("/cart", cartRoute);