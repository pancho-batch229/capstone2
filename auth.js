/* -- AUTHENTICATION TOKENS -- */
const jwt = require('jsonwebtoken');

const secretKey = 'ECommerceAPI';

module.exports.create = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secretKey, {});
}

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization

	if(typeof token !== 'undefined') {
		token = token.slice(7, token.length);
		// removes the bearer prefix

		return jwt.verify(token, secretKey, (error, data) => {
			// validates the token by decrypting the token using the secret code
			if(error) {
				return res.send({ auth: "failed - token is not valid" });

			} else {
				next();

			}
		});
	} else {
		return res.send({ auth: "failed - token does not exist" });
		
	}
}

module.exports.decode = (token) => {
	if(typeof token !== 'undefined') {
		token = token.slice(7, token.length);

		return jwt.verify(token, secretKey, (error, data) => {
			if(error) {
				return res.send({ auth: "failed - token is not valid" });

			} else {
				return jwt.decode(token, {complete: true}).payload;

			}
		});
	} else {
		return null;

	}
}
