/* -- PRODUCT MODEL -- */
const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		unique: true,
		required: [true, 'Product name is required.']
	},
	price: {
		type: Number,
		required: [true, 'Product price is required.']
	},
	description: {
		type: String,
		required: [true, 'Product description is required.']
	},
	isActive: {
		type: Boolean,
		default: true
	}
}, { timestamps: true });

module.exports = mongoose.model('Product', productSchema);
