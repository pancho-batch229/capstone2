/* -- USER CONTROLLER -- */
const express = require('express');
const auth = require('../auth');
const bcrypt = require('bcrypt');
const User = require('../models/User');

// --------------------------------------------------------------------------------- //
// User Registration
module.exports.registerUser = (reqBody, res) => {
    return User.find({ email: reqBody.email }).then(result => {
        if(result.length > 0) {
            return 'message: The email address entered is already registered.';

        } else {
            let newUser = new User({
                email: reqBody.email,
                password: bcrypt.hashSync(reqBody.password, 10) 

            });
    
            return newUser.save().then((user, error) => {
                if(error) {
                    console.log(error);
                    return false;

                } else {
                    return 'message: Email address has been successfully registered!';

                }
            });
        }
    });
}

// --------------------------------------------------------------------------------- //
// All Users
module.exports.allUsers = (data) => {
    return User.find({ _id: { $ne: data.userId } }).then(result => {
        if(!data.adminToken) {
            return false;

        }

        return result;
        
    });
}


// --------------------------------------------------------------------------------- //
// User Login
module.exports.loginUser = (reqBody) => {
    return User.findOne({ email: reqBody.email }).then(result => {
        if(result == null) {
            return 'message: Email does not exist, please register.';

        } else {
            const match = bcrypt.compareSync(reqBody.password, result.password);

            if(!match) {
                return 'message: Incorrect password!!';

            } else {
                return `access token: ${auth.create(result)}`;

            }
        }
    });
}

// --------------------------------------------------------------------------------- //
// User Profile 
module.exports.userProfile = (data) => {
    return User.findById(data.userId).then(result => {
        if(data.userToken !== data.userId) {
            return 'message: Unauthorized user detcted!';

        }

        result.password = "";

        return result;

    });
}

// --------------------------------------------------------------------------------- //
// Set Admin
module.exports.setAdmin = async (data) => {
    if(!data.adminToken) {
        return 'message: Unauthorized user detcted!';

    }

    try {
        await User.findByIdAndUpdate(data.reqParams.userId, { isAdmin: data.isAdmin });
        
        return 'message: User privilages updated.';

    } catch (error) {
        console.log(error);
        return false;
        
    }
}