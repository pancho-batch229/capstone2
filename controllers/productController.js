/* -- PRODUCT CONTROLLER -- */
const Product = require('../models/Product');
const User = require('../models/User');

// --------------------------------------------------------------------------------- //
// Create Product
module.exports.postProduct = async (reqBody) => {
	if(reqBody.isAdmin) {
		let existingProduct = await Product.findOne({ name: reqBody.product.name }).collation({ locale: 'en', strength: 2 });

		if(existingProduct) {
			return 'message: Product with that name already exists.';

		} else {
			let newProduct = new Product({
                name: reqBody.product.name,
                description: reqBody.product.description,
                price: reqBody.product.price

            });

            try {
            	await newProduct.save();

            	return 'message: Product successfuly posted.';

            } catch(error) {
            	return error;

            }
		}

	} else {
		return 'message: Unauthorized user detcted!';

	}
}


// --------------------------------------------------------------------------------- //
// All Products
module.exports.allProducts = () => {
	return Product.find({ isActive: true }).then(result => {
		return result;

	});
}

// --------------------------------------------------------------------------------- //
// All Products(ADMIN)
module.exports.allProductsAdmin = () => {
	return Product.find({}).then(result => {
		return result;

	});
}

// --------------------------------------------------------------------------------- //
// Product Details
module.exports.productDetail = (id) => {
	return Product.findById(id.productId)
		.then(product => {
			if(!product) {
				return 'error: Product not found!';

			} else {
				return product;

			}
		});
}

// --------------------------------------------------------------------------------- //
// Update Product
module.exports.updateProduct = async (data) => {
	if(!data.isAdmin) {
		return 'error: Unauthorized user detcted!';

	}

	try {
		const product = await Product.findById(data.id.productId);

		if(!product) {
			return 'error: Product not found!';

		}

		const existingProduct = await Product.findOne({
			name: data.product.name,
			_id: { $ne: data.id.productId }

		}).collation({ locale: 'en', strength: 2 });

		if(existingProduct) {
			return 'message: Product with that name already exists.';

		}

		await Product.findByIdAndUpdate(data.id.productId, data.product);

		return 'message: Product updated!';

	} catch(error) {
		return error;

	}
}

// --------------------------------------------------------------------------------- //
// Archive Product
module.exports.archiveProduct = async (data) => {
	if(data.isAdmin) {
		let updateActiveField = { isActive : data.product.isActive };

    	try {
    		await Product.findByIdAndUpdate(data.reqParams.productId, updateActiveField);

    		return 'message: Product archived!';

    	} catch(error) {
    		return error;

    	}

	} else {
		return 'message: Unauthorized user detcted!';

	}
}
