/* -- CART CONTROLLER -- */
const Product = require('../models/Product');
const User = require('../models/User');
const Cart = require('../models/Cart');

// --------------------------------------------------------------------------------- //
// User Cart
module.exports.userCart = async ({ userId, userToken }) => {
	if(userToken !== userId) {
		return false;

	}

	const cart = Cart.find({ userId });

	return cart;
}


// --------------------------------------------------------------------------------- //
// Add To Cart
module.exports.addToCart = async ({ productId, quantity, userToken, userId }) => {
	if(!productId || !quantity) {
		return 'error: productId and quantity are required.';

	}

	if(userToken !== userId) {
		return 'error: Unauthorized user detcted!';

	}

	try {
		const product = await Product.findOne({ _id: productId, isActive: true})

		if(!product) {
			return 'error: Product does not exist.';

		}

		let cart = await Cart.findOne({ userId });
		if(!cart){
			cart = new Cart({
				products:[],
				totalAmount: 0,
				userId: userId

			});
		}

		let existingProductIndex = cart.products.findIndex(p => p.productId === productId);

		if(existingProductIndex >= 0) {
			cart.products[existingProductIndex].quantity += quantity;
			cart.products[existingProductIndex].subtotal = cart.products[existingProductIndex].quantity * product.price;

		} else {
			cart.products.push({
				productId,
				name: product.name,
				quantity,
				price: product.price,
				subtotal: quantity * product.price

			});

		}

		cart.totalAmount = cart.products.reduce((total, p) => total + p.subtotal, 0);
		
		await cart.save();

		return 'message: Product added to cart!';

	} catch (error) {
		return error;

	}
}

// --------------------------------------------------------------------------------- //
// Modify Quantity
module.exports.updateProductQuantity = async({ productId, quantity, userToken, userId }) => {

	if(!productId || !quantity) {
		return 'error: productId and quantity are required.';

	}

	if(userToken !== userId) {
		return 'error: Unauthorized user detcted!';

	}

	try {
		const product = await Product.findById(productId);

		if(!product) {
			return 'error: Product does not exist.';

		}

		let cart = await Cart.findOne({ userId });

		if(!cart) {
			return 'error: Cart does not exist.';

		}

		let existingProductIndex = cart.products.findIndex(p => p.productId === productId);

		if (existingProductIndex >= 0) {
			cart.products[existingProductIndex].quantity = quantity;
			cart.products[existingProductIndex].subtotal = cart.products[existingProductIndex].quantity * product.price;
			cart.totalAmount = cart.products.reduce((total, p) => total + p.subtotal, 0);

			await cart.save();

			return 'message: Product quantity updated!';

		} else {
			return 'error: Product not found in cart.';

		}
		
	} catch(error) {
		return error;

	}
}

// --------------------------------------------------------------------------------- //
// Remove Product
module.exports.removeProductFromCart = async ({ productId, userToken, userId}) => {

	if(!productId) {
		return 'error: productId is required.';

	}

	if(userToken !== userId) {
		return 'error: Unauthorized user detcted!';

	}

	try {
		let cart = await Cart.findOne({ userId })

		if(!cart) {
			return 'message: Cart does not exist!';

		}

		let existingProductIndex = cart.products.findIndex(p => p.productId === productId);

		if(existingProductIndex >= 0) {
			const product = cart.products.splice(existingProductIndex, 1)
			cart.totalAmount = cart.products.reduce((total, p) => total + p.subtotal, 0)
			
			await cart.save()

			return `message:Product ${productId} has been removed from cart`;

		} else {
			return 'error: Product not found in cart';

		}

	} catch(error) {
		return error;

	}
}